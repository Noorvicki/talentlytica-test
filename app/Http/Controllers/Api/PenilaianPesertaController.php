<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Helpers\Peserta as PesertaHelper;
use App\Helpers\PenilaianPeserta as PenilaianPesertaHelper;
use App\Http\Requests\Penilaian\PenilaianRequest;
use App\Http\Requests\Peserta\PesertaRequest;
use App\Http\Resources\PenilaianPeserta\LaporanPenilaianPesertaResource;
use App\Http\Resources\PenilaianPeserta\PenilaianPesertaCollection;
use App\Http\Resources\PenilaianPeserta\PenilaianPesertaResource;

use Illuminate\Http\Request;

class PenilaianPesertaController extends Controller
{
    private $pesertaHelper, $penilaianPesertaHelper;

    public function __construct()
    {
        $this->pesertaHelper = new PesertaHelper();
        $this->penilaianPesertaHelper = new PenilaianPesertaHelper();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = [
            'nama' => $request->nama ?? '',
            'email' => $request->email ?? '',
            'nilaiX' => $request->nilaiX ?? '',
            'nilaiY' => $request->nilaiY ?? '',
            'nilaiZ' => $request->nilaiZ ?? '',
            'nilaiW' => $request->nilaiW ?? '',
        ];

        $penilaianPeserta = $this->penilaianPesertaHelper->getAll($filter);

        return response()->success(new PenilaianPesertaCollection($penilaianPeserta));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PesertaRequest $pesertaRequest, PenilaianRequest $penilaianRequest)
    {
        if (isset($pesertaRequest->validator) && $pesertaRequest->validator->fails()) {
            return response()->failed($pesertaRequest->validator->errors());
        }

        if (isset($penilaianRequest->validator) && $penilaianRequest->validator->fails()) {
            return response()->failed($penilaianRequest->validator->errors());
        }

        $inputPeserta = $pesertaRequest->only(['nama', 'email']);
        $inputPenilaian = $pesertaRequest->only(['nilaiX', 'nilaiY', 'nilaiZ', 'nilaiW']);

        // //create peserta
        $peserta = $this->pesertaHelper->create($inputPeserta);
        if (!$peserta['status']) {
            return response()->failed($peserta['error'], 422);
        }

        // //create penilaian peserta
        $inputPenilaian['m_peserta_id'] = $peserta['data']['id'];
        $penilaianPeserta = $this->penilaianPesertaHelper->create($inputPenilaian);
        if (!$penilaianPeserta['status']) {
            return response()->failed($penilaianPeserta['error'], 422);
        }

        //return response
        return response()->success(new PenilaianPesertaResource($penilaianPeserta['data']), 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Peserta  $peserta
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataUser = $this->penilaianPesertaHelper->getById($id);

        if (empty($dataUser)) {
            return response()->failed(['Data user tidak ditemukan']);
        }
        return response()->success(new LaporanPenilaianPesertaResource($dataUser));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Peserta  $peserta
     * @return \Illuminate\Http\Response
     */
    public function update(PesertaRequest $pesertaRequest, PenilaianRequest $penilaianRequest)
    {
        if (isset($pesertaRequest->validator) && $pesertaRequest->validator->fails()) {
            return response()->failed($pesertaRequest->validator->errors());
        }

        if (isset($penilaianRequest->validator) && $penilaianRequest->validator->fails()) {
            return response()->failed($penilaianRequest->validator->errors());
        }

        $inputPeserta = $pesertaRequest->only(['nama', 'email']);
        $inputPenilaian = $pesertaRequest->only(['nilaiX', 'nilaiY', 'nilaiZ', 'nilaiW']);
        // //update peserta
        $peserta = $this->pesertaHelper->update($inputPeserta, $pesertaRequest->m_peserta_id);
        if (!$peserta['status']) {
            return response()->failed($peserta['error'], 422);
        }

        // //update penilaian peserta
        $penilaianPeserta = $this->penilaianPesertaHelper->update($inputPenilaian, $penilaianRequest->id);
        if (!$penilaianPeserta['status']) {
            return response()->failed($penilaianPeserta['error'], 422);
        }
        return response()->success(new PenilaianPesertaResource($penilaianPeserta['data']), 'Data penilaian peserta berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Peserta  $peserta
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penilaianPeserta = $this->penilaianPesertaHelper->delete($id);

        if (!$penilaianPeserta) {
            return response()->failed(['Mohon maaf data penilaian peserta tidak ditemukan']);
        }

        return response()->success($penilaianPeserta, 'Data penilaian peserta telah dihapus');
    }
}
