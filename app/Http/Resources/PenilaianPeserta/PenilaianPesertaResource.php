<?php

namespace App\Http\Resources\PenilaianPeserta;

use Illuminate\Http\Resources\Json\JsonResource;

class PenilaianPesertaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->resource->id,
            'nama'         => $this->resource->peserta->nama,
            'email'        => $this->resource->peserta->email,
            'nilaiX'       => $this->resource->nilaiX,
            'nilaiY'       => $this->resource->nilaiY,
            'nilaiZ'       => $this->resource->nilaiZ,
            'nilaiW'       => $this->resource->nilaiW,
            'm_peserta_id' => $this->resource->m_peserta_id,
        ];
    }
}
