<?php

namespace App\Http\Resources\PenilaianPeserta;

use Illuminate\Http\Resources\Json\JsonResource;

class LaporanPenilaianPesertaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'id'           => $this->resource->id,
            'nama'         => $this->resource->peserta->nama,
            'email'        => $this->resource->peserta->email,
            'intelegensi'       => $this->map(((40 / 100 * $this->resource->nilaiX + 60 / 100 * $this->resource->nilaiY) / 2), 0.5, 13.5, 1, 5),
            'numericalAbility'  => $this->map(((30 / 100 * $this->resource->nilaiZ + 70 / 100 * $this->resource->nilaiW) / 2), 0.5, 7.25, 1, 5),
            'm_peserta_id' => $this->resource->m_peserta_id,
        ];
    }

    public function map($x, $in_min, $in_max, $out_min, $out_max)
    {
        return round(($x - $in_min) * ($out_max - $out_min) / ($in_max - $in_min) + $out_min);
    }
}
