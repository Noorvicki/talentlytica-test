<?php

namespace App\Helpers;

use App\Models\Peserta as PesertaModel;

class Peserta
{
    private $pesertaModel;

    public function __construct()
    {
        $this->pesertaModel = new PesertaModel();
    }

    public function getAll(array $filter, int $itemPerPage = 0, string $sort = ''): object
    {
        $peserta = $this->pesertaModel::query();

        if (!empty($filter['nama'])) {
            $peserta->where('nama', 'LIKE', '%'.$filter['nama'].'%');
        }

        if (!empty($filter['email'])) {
            $peserta->where('email', 'LIKE', '%'.$filter['email'].'%');
        }

        $sort = $sort ?: 'id DESC';
        $peserta->orderByRaw($sort);
        $itemPerPage = ($itemPerPage > 0) ? $itemPerPage : false ;

        return $peserta->paginate($itemPerPage)->appends('sort', $sort);
    }

    public function getById(int $id): object
    {
        return $this->pesertaModel::find($id);
    }

    public function create(array $payload): array
    {
        try {

            $peserta = $this->pesertaModel::create($payload);
            return [
                'status' => true,
                'data' => $peserta
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    public function update(array $payload, int $id): array
    {
        try {
            $peserta = $this->pesertaModel::find($id);

            $peserta->update($payload);

            return [
                'status' => true,
                'data' => $this->getById($id)
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    function delete(int $id): bool
    {
        try {
            $this->pesertaModel::find($id)->delete();
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }

}
