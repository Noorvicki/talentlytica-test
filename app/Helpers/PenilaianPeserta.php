<?php

namespace App\Helpers;

use App\Models\PenilaianPeserta as PenilaianPesertaModel;

class PenilaianPeserta
{
    private $penilaianPesertaModel;

    public function __construct()
    {
        $this->penilaianPesertaModel = new PenilaianPesertaModel();
    }

    public function getAll(array $filter, int $itemPerPage = 0, string $sort = ''): object
    {
        $penilaianPeserta = $this->penilaianPesertaModel::query();

        $penilaianPeserta->with(["peserta" => function($q) use($filter){
            if(!empty($filter['nama'])){
                $q->where('peserta.nama', 'LIKE',"%".$filter['nama']."%");
            }
            if(!empty($filter['email'])){
                $q->where('peserta.email', $filter['email']);
            }
        }]);
        if (!empty($filter['nilaiX'])) {
            $penilaianPeserta->where('nilaiX', $filter['nilaiX']);
        }
        if (!empty($filter['nilaiY'])) {
            $penilaianPeserta->where('nilaiY', $filter['nilaiY']);
        }
        if (!empty($filter['nilaiZ'])) {
            $penilaianPeserta->where('nilaiZ', $filter['nilaiZ']);
        }
        if (!empty($filter['nilaiW'])) {
            $penilaianPeserta->where('nilaiW', $filter['nilaiW']);
        }
        if (!empty($filter['m_peserta_id'])) {
            $penilaianPeserta->where('m_peserta_id', $filter['m_peserta_id']);
        }

        $sort = $sort ?: 'id DESC';
        $penilaianPeserta->orderByRaw($sort);
        $itemPerPage = ($itemPerPage > 0) ? $itemPerPage : false ;

        return $penilaianPeserta->paginate($itemPerPage)->appends('sort', $sort);
    }

    public function getById(int $id): object
    {
        return $this->penilaianPesertaModel::find($id);
    }

    public function create(array $payload): array
    {
        try {

            $penilaianPeserta = $this->penilaianPesertaModel::create($payload);
            return [
                'status' => true,
                'data' => $penilaianPeserta
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    public function update(array $payload, int $id): array
    {
        try {
            $penilaianPeserta = $this->penilaianPesertaModel::find($id);

            $penilaianPeserta->update($payload);

            return [
                'status' => true,
                'data' => $this->getById($id)
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    function delete(int $id): bool
    {
        try {
            $this->penilaianPesertaModel::find($id)->delete();
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }

}
