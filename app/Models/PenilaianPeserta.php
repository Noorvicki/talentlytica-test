<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PenilaianPeserta extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 't_penilaian_peserta';

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'nilaiX',
        'nilaiY',
        'nilaiZ',
        'nilaiW',
        'm_peserta_id',
    ];

    public function peserta()
    {
        return $this->hasOne(Peserta::class, 'id', 'm_peserta_id')->whereNull('deleted_at');
    }
}
