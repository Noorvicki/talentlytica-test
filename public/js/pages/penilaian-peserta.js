var dataIndex = [];
var current_page = 1;
var records_per_page = 5;

$(document).ready(function () {
    emptyForm();
    getData();
    submitButton(false);
});

function prevPage() {
    if (current_page > 1) {
        current_page--;
        changePage(current_page);
    }
}

function nextPage() {
    if (current_page < numPages()) {
        current_page++;
        changePage(current_page);
    }
}

function changePage(page) {
    var btn_next = document.getElementById("btn_next");
    var btn_prev = document.getElementById("btn_prev");
    var listing_table = document.getElementById("main-table");

    // Validate page
    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();

    listing_table.innerHTML = "";

    for (var i = (page - 1) * records_per_page; i < (page * records_per_page) && i < dataIndex.length; i++) {
        listing_table.innerHTML += '<tr>' +
            '<td>' + dataIndex[i].nama + '</td>' +
            '<td class="text-center">' + dataIndex[i].email + '</td>' +
            '<td class="text-center">' + dataIndex[i].nilaiX + '</td>' +
            '<td class="text-center">' + dataIndex[i].nilaiY + '</td>' +
            '<td class="text-center">' + dataIndex[i].nilaiZ + '</td>' +
            '<td class="text-center">' + dataIndex[i].nilaiW + '</td>' +
            '<td class="text-center">' +
            '<div class="btn-group">' +
            '<a onclick="getLaporanData(' + dataIndex[i].id + ')" data-toggle="modal" data-target="#laporanModal" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Lihat Laporan"><i class=" ion-ios-stats-outline"></i></a>' +
            '<a onclick="editData(' + i + ')" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit"><i class=" ion-ios-create-outline"></i></a>' +
            '<a onclick="deleteData(' + dataIndex[i].id + '); event.stopPropagation();" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Hapus"><i class=" ion-ios-trash-outline"></i></a>' +
            '</div>' +
            '</td>' +
            '</tr>';
    }

    if (page == 1) {
        btn_prev.style.pointerEvents = "none";
        btn_prev.style.opacity = "50%";
    } else {
        btn_prev.style.pointerEvents = "auto";
        btn_prev.style.opacity = "100%";
    }

    if (page == numPages()) {
        btn_next.style.pointerEvents = "none";
        btn_next.style.opacity = "50%";
    } else {
        btn_next.style.pointerEvents = "auto";
        btn_next.style.opacity = "100%";
    }
}

function numPages() {
    return Math.ceil(dataIndex.length / records_per_page);
}

function getData() {
    $.ajax({
        url: `/api/penilaian-peserta`,
        type: "GET",
        success: function (response) {
            dataIndex = response.data;
            changePage(current_page);
        },
        error: function (error) {
            console.log(response);
        }
    });
}

function getLaporanData(id) {
    var isiLaporanTable = '';
    $('#laporan-table').empty();
    $('#nama-modal').empty();
    $('#email-modal').empty();
    $.ajax({
        url: `/api/penilaian-peserta/` + id,
        type: "GET",
        success: function (response) {

            isiLaporanTable += '<tr><td>Aspek Intelegensi</td>';
            for (let i = 1; i <= 5; i++) {
                isiLaporanTable += '<td class="text-center">' + (response.data.intelegensi == i ? '<i class="ion-md-checkmark"></i>' : '') + '</td>';
            }
            isiLaporanTable += '</tr>';

            isiLaporanTable += '<tr><td>Aspek Numerical Ability</td>';
            for (let i = 1; i <= 5; i++) {
                isiLaporanTable += '<td class="text-center">' + (response.data.numericalAbility == i ? '<i class="ion-md-checkmark"></i>' : '') + '</td>';
            }
            isiLaporanTable += '</tr>';

            $('#laporan-table').append(isiLaporanTable)
            $('#nama-modal').append(response.data.nama)
            $('#email-modal').append(response.data.email)
            console.log(isiLaporanTable);

        },
        error: function (error) {
            console.log(response);
        }
    });
}

function submitButton(isedit) {
    if (!isedit) {
        $('#submit-button').empty()
        $('#submit-button').append('<a onclick=" submitData(); event.stopPropagation();" class="btn btn-sm btn-primary w-100">Tambahkan Data</a>')
    } else {
        $('#submit-button').empty()
        $('#submit-button').append(
            '<a onclick=" emptyForm(); submitButton(false); event.stopPropagation();"class="btn btn-sm btn-secondary" style="width: 49%;">Batalkan</a>' +
            '<a onclick=" submitData(); event.stopPropagation();"class="btn btn-sm btn-primary" style="width: 49%;">Simpan Perubahan</a>'
        )
    }
}

function emptyForm() {
    $('#id').val(null);
    $('#nama').val(null);
    $('#email').val(null);
    $('#m_peserta_id').val(null);
    $('#nilaiX').val(null);
    $('#nilaiY').val(null);
    $('#nilaiZ').val(null);
    $('#nilaiW').val(null);
}

function submitData() {
    formData = {
        id: $('#id').val(),
        nama: $('#nama').val(),
        email: $('#email').val(),
        m_peserta_id: $('#m_peserta_id').val(),
        nilaiX: $('#nilaiX').val(),
        nilaiY: $('#nilaiY').val(),
        nilaiZ: $('#nilaiZ').val(),
        nilaiW: $('#nilaiW').val(),
        _token: $("meta[name='csrf-token']").attr("content"),
    }


    if (formData.id > 0) {
        $.ajax({
            url: `/api/penilaian-peserta`,
            type: "PUT",
            cache: false,
            data: formData,
            success: function (response) {
                getData();
                toast('success', 'Data Berhasil Diubah')
                emptyForm();
                submitButton(false);
            },
            error: function (error) {
                Swal.fire({
                    title: 'Gagal',
                    text: error.responseJSON.errors,
                    icon: 'warning',
                    confirmButtonText: 'Kembali',
                })
            }
        });
    } else {
        $.ajax({
            url: `/api/penilaian-peserta`,
            type: "POST",
            cache: false,
            data: formData,
            success: function (response) {
                getData();
                toast('success', 'Data Berhasil Ditambahkan')
                emptyForm();
                submitButton(false);
            },
            error: function (error) {
                Swal.fire({
                    title: 'Gagal',
                    text: error.responseJSON.errors,
                    icon: 'warning',
                    confirmButtonText: 'Kembali',
                })
            }
        });
    }
}

function editData(i) {
    this.submitButton(true);
    var val_ = dataIndex[i]
    $('#id').val(val_.id);
    $('#nama').val(val_.nama);
    $('#email').val(val_.email);
    $('#m_peserta_id').val(val_.m_peserta_id);
    $('#nilaiX').val(val_.nilaiX);
    $('#nilaiY').val(val_.nilaiY);
    $('#nilaiZ').val(val_.nilaiZ);
    $('#nilaiW').val(val_.nilaiW);
}

function deleteData(id) {
    Swal.fire({
        title: 'Perhatian',
        text: 'data yang akan dihapus mungkin berpengaruh terhadap data lain',
        icon: 'warning',
        confirmButtonText: 'Tetap Hapus',
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: `/api/penilaian-peserta/` + id,
                type: "DELETE",
                success: function (response) {
                    toast('success', 'Data Berhasil Dihapus');
                    getData();
                },
                error: function (error) {
                    Swal.fire({
                        title: 'Gagal',
                        text: error.responseJSON.errors,
                        icon: 'warning',
                        confirmButtonText: 'Kembali',
                    })
                }
            });

        }
    })
}

function toast(icon, title) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    Toast.fire({
        icon: icon,
        title: title
    })
}
