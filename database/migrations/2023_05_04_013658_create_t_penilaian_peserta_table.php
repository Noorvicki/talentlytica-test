<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTPenilaianPesertaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_penilaian_peserta', function (Blueprint $table) {
            $table->id();
            $table->integer('m_peserta_id');
            $table->integer('nilaiX');
            $table->integer('nilaiY');
            $table->integer('nilaiZ');
            $table->integer('nilaiW');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_penilaian_peserta');
    }
}
