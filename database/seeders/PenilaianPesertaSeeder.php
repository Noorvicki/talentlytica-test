<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Helpers\Peserta as PesertaHelper;
use App\Helpers\PenilaianPeserta as PenilaianPesertaHelper;

class PenilaianPesertaSeeder extends Seeder
{
    private $pesertaHelper, $penilaianPesertaHelper;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct()
    {
        $this->pesertaHelper = new PesertaHelper();
        $this->penilaianPesertaHelper = new PenilaianPesertaHelper();
    }
    public function run()
    {

        $faker = Faker::create('id_ID');

        for ($i = 1; $i <= 10; $i++) {

            $inputPeserta = [
                'nama' => $faker->name,
                'email' => $faker->email,
            ];
            $peserta = $this->pesertaHelper->create($inputPeserta);

            $inputPenilaian = [
                'm_peserta_id' => $peserta['data']['id'],
                'nilaiX' => $faker->numberBetween(1, 33),
                'nilaiY' => $faker->numberBetween(1, 23),
                'nilaiZ' => $faker->numberBetween(1, 18),
                'nilaiW' => $faker->numberBetween(1, 13)
            ];
            $penilaianPeserta = $this->penilaianPesertaHelper->create($inputPenilaian);
        }
    }
}
