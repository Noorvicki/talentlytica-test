<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/penilaian-peserta', [App\Http\Controllers\Api\PenilaianPesertaController::class, 'index'])->name('get-penilaian-peserta');
Route::post('/penilaian-peserta', [App\Http\Controllers\Api\PenilaianPesertaController::class, 'store'])->name('store-penilaian-peserta');
Route::put('/penilaian-peserta', [App\Http\Controllers\Api\PenilaianPesertaController::class, 'update'])->name('update-penilaian-peserta');
Route::get('/penilaian-peserta/{id}', [App\Http\Controllers\Api\PenilaianPesertaController::class, 'show'])->name('show-penilaian-peserta');
Route::delete('/penilaian-peserta/{id}', [App\Http\Controllers\Api\PenilaianPesertaController::class, 'destroy'])->name('delete-penilaian-peserta');
