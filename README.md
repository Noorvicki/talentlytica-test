# Talentlytica Test App

This repo is functionality complete — PRs and issues welcome!

----------

# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/8.x)


Clone the repository

    git clone https://gitlab.com/Noorvicki/talentlytica-test.git

Switch to the repo folder

    cd talentlytica-test

Install all the dependencies using composer

    composer install

Install npm dependencies and build auth component

    npm install && npm run dev

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

## Database seeding

***Note*** : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command

    php artisan migrate:refresh 
    
Run the database seeder and you're done

    php artisan db:seed

----------

# Code overview
## Custom Folders

- `app/Http/Controllers/Api` - Contains all the api controllers
- `app/Http/Controllers/Web` - Contains all the web controllers
- `app/Http/Helper` - Contains all the custom helpers
- `app/Http/Requests` - Contains all the requests handler
- `app/Http/Resources` - Contains all the api resources

## Environment variables

- `.env` - Environment variables can be set in this file

***Note*** : You can quickly set the database information and other variables in this file and have the application fully working.

----------

# Running APP

Run the laravel development server

    php artisan serve

The api can now be accessed at

    http://localhost:8000


----------


