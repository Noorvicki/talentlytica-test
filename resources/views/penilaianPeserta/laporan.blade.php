<div class="modal fade " id="laporanModal" tabindex="-1" role="dialog" aria-labelledby="laporanModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <span class="w-100 text-center">
                    <h2 id="nama-modal" class="modal-title font-weight-bold"></h2>
                    <p id="email-modal" class="modal-title font-italic"></p>
                </span>
            </div>
            <div class="modal-body">
                <table class="table table-striped m-0">
                    <thead class="bg-primary text-white">
                        <tr>
                            <th class="text-left align-middle col" rowspan="2">Aspek</th>
                            <th class="text-center align-middle" style="width: 75px;">1</th>
                            <th class="text-center align-middle" style="width: 75px;">2</th>
                            <th class="text-center align-middle" style="width: 75px;">3</th>
                            <th class="text-center align-middle" style="width: 75px;">4</th>
                            <th class="text-center align-middle" style="width: 75px;">5</th>
                        </tr>
                    </thead>
                    <tbody id='laporan-table'>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
