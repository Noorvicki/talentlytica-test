<form>
    <div class="form-group row mx-0 mb-2">
        <label for="nama" class="p-0 col-3">Nama</label>
        <input value="" type="text" class="py-0 px-1 form-control col" id="nama" placeholder="masukan nama">
    </div>
    <div class="form-group row mx-0 mb-2">
        <label for="email" class="p-0 col-3">Email</label>
        <input value="" type="email" class="py-0 px-1 form-control col" id="email" placeholder="masukan email">
    </div>
    <hr>
    <div class="form-group row mx-0 mb-2">
        <label for="nilaiX" class="p-0 col-3">Nilai X</label>
        <input value="" type="number" min="1" max="33" class="py-0 px-1 form-control col" id="nilaiX" placeholder="masukan nilai x">
    </div>
    <div class="form-group row mx-0 mb-2">
        <label for="nilaiY" class="p-0 col-3">Nilai Y</label>
        <input value="" type="number" min="1" max="23" class="py-0 px-1 form-control col" id="nilaiY" placeholder="masukan nilai y">
    </div>
    <div class="form-group row mx-0 mb-2">
        <label for="nilaiZ" class="p-0 col-3">Nilai Z</label>
        <input value="" type="number" min="1" max="18" class="py-0 px-1 form-control col" id="nilaiZ" placeholder="masukan nilai z">
    </div>
    <div class="form-group row mx-0 mb-2">
        <label for="nilaiW" class="p-0 col-3">Nilai W</label>
        <input value="" type="number" min="1" max="13" class="py-0 px-1 form-control col" id="nilaiW" placeholder="masukan nilai w">
    </div>
    <hr>
    <input value="" type="hidden" id="id">
    <input value="" type="hidden" id="m_peserta_id">
    <div id="submit-button" class="d-flex justify-content-between"></div>
</form>
