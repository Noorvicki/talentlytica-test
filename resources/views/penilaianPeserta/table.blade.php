<table class="table table-striped m-0">
    <thead class="bg-primary text-white">
        <tr>
            <th class="align-middle col" rowspan="2">Nama</th>
            <th class="text-center align-middle col-2" rowspan="2">Email</th>
            <th class="text-center align-middle" colspan="4">Nilai</th>
            <th class="text-center align-middle col-2" rowspan="2">Action</th>
        </tr>
        <tr>
            <th class="text-center align-middle" style="width: 75px;">X</th>
            <th class="text-center align-middle" style="width: 75px;">Y</th>
            <th class="text-center align-middle" style="width: 75px;">Z</th>
            <th class="text-center align-middle" style="width: 75px;">W</th>
        </tr>
    </thead>
    <tbody id='main-table'>
    </tbody>
</table>
<div>
    <nav>
        <ul class="pagination my-2 justify-content-center">
            <li class="page-item"><a class="page-link" href="javascript:prevPage()" id="btn_prev">Previous</a></li>
            <li class="page-item"><a class="page-link" href="javascript:nextPage()" id="btn_next">Next</a></li>
        </ul>
    </nav>
</div>
