@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mx-2">
        <div class="col-sm-3 ps-0 pe-1">
            <div class="card p-2 shadow-sm border-0">
                @include('penilaianPeserta/form')
            </div>
        </div>
        <div class="col-sm-9 pe-0 ps-1 ">
            <div class="card p-0 shadow-sm border-0">
                @include('penilaianPeserta/table')
            </div>
        </div>
    </div>
</div>
@include('penilaianPeserta/laporan')
@endsection
@section('js')
<script src="{{ asset('js/pages/penilaian-peserta.js') }}" defer></script>
@endsection
